//
//  Grid.swift
//  singleton-lista-contactos
//
//  Created by Developer on 19/07/18.
//  Copyright © 2018 adolx. All rights reserved.
//

import UIKit

class Grid: UIViewController , UICollectionViewDelegate , UICollectionViewDataSource {
    
    
    
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return(Singleton.shared.count)
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
       
        if Singleton.shared[indexPath.row].Image != nil
        {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cell", for: indexPath) as! Mycell
            cell.MyImage.image = Singleton.shared[indexPath.row].Image
            cell.Myname.text = Singleton.shared[indexPath.row].Nome
            cell.MyPhone.text = Singleton.shared[indexPath.row].Tel
            return cell
        }
        else
        {
        
            let cell2 = collectionView.dequeueReusableCell(withReuseIdentifier: "cell2", for: indexPath) as! MyCellType2
            cell2.MyName.text = Singleton.shared[indexPath.row].Nome
            cell2.MyApelido.text = Singleton.shared[indexPath.row].Sobrenome
            cell2.Myphone.text = Singleton.shared[indexPath.row].Tel
            return cell2
        }
    }

    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let storyBoard = UIStoryboard(name: "Main", bundle: nil)
        let dvc3 = storyBoard.instantiateViewController(withIdentifier: "ViewController3") as! ViewController3
        
        dvc3.nome3 = Singleton.shared[indexPath.row].Nome
        dvc3.sobrenome3 = Singleton.shared[indexPath.row].Sobrenome
        dvc3.tel3 = Singleton.shared[indexPath.row].Tel
        if (!(Singleton.shared[indexPath.row].Image == nil)){
            dvc3.img3 = Singleton.shared[indexPath.row].Image!
            dvc3.aux = indexPath.row
        }
        self.navigationController?.pushViewController(dvc3, animated: true)
    }
    @IBAction func GoList(_ sender: Any) {
        let storyBoard = UIStoryboard(name: "Main", bundle: nil)
        let go = storyBoard.instantiateViewController(withIdentifier: "ViewController2") as! ViewController2
        self.navigationController?.pushViewController(go, animated: true)
        
        
    }
    override func viewDidLoad() {
        super.viewDidLoad()

        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        
    }
    


}
