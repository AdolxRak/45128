//
//  ViewController3.swift
//  singleton-lista-contactos
//
//  Created by Developer on 12/07/2018.
//  Copyright © 2018 adolx. All rights reserved.
//

import UIKit

class ViewController3: UIViewController,UIImagePickerControllerDelegate,UINavigationControllerDelegate {

    @IBOutlet weak var _nomeVC3: UILabel!
    @IBOutlet weak var _sobrenomeVC3: UILabel!
    @IBOutlet weak var _telVC3: UILabel!
    @IBOutlet weak var _imgVC3: UIImageView!
    
    @IBOutlet weak var _nomeText3: UITextField!
    @IBOutlet weak var _sobrenomeText3: UITextField!
    @IBOutlet weak var _telText3: UITextField!
    
    var nome3 = String()
    var sobrenome3 = String()
    var tel3 = String()
    var img3 = UIImage()
    
    var aux: Int = 0
    var adr : UIImage!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        _nomeVC3.text = nome3
        _sobrenomeVC3.text = sobrenome3
        _telVC3.text = tel3
        _imgVC3.layer.cornerRadius = _imgVC3.frame.height/2
        _imgVC3.clipsToBounds = true
        _imgVC3.image = img3
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func btnVoltar(_ sender: Any) {
        performSegue(withIdentifier: "seguer1", sender: self)
    }
    

    @IBAction func btnEditar(_ sender: Any) {
        
        _nomeVC3.text = _nomeText3.text!
        _telVC3.text = _telText3.text!
        _sobrenomeVC3.text = _sobrenomeText3.text!
        
        
        Singleton.shared[aux].Nome = _nomeVC3.text!
        Singleton.shared[aux].Tel = _telVC3.text!
        Singleton.shared[aux].Sobrenome = _sobrenomeVC3.text!
        Singleton.shared[aux].Image = _imgVC3.image
        _imgVC3.image = Singleton.shared[aux].Image
    }
    
    @IBAction func btnCamera(_ sender: Any) {
        if UIImagePickerController.isSourceTypeAvailable(.camera){
            let imagePicker = UIImagePickerController()
            imagePicker.delegate = self as UIImagePickerControllerDelegate & UINavigationControllerDelegate
            imagePicker.allowsEditing = false
            imagePicker.sourceType = UIImagePickerControllerSourceType.camera
            self.present(imagePicker,animated: false,completion: nil)
            
        }
    }
    
    
    @IBAction func btnImages(_ sender: Any) {
        let imagePicker = UIImagePickerController()
        imagePicker.delegate = self as UIImagePickerControllerDelegate & UINavigationControllerDelegate
        imagePicker.allowsEditing = false
        imagePicker.sourceType = UIImagePickerControllerSourceType.photoLibrary
        self.present(imagePicker,animated: false,completion: nil)
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        
        if let image = info[UIImagePickerControllerOriginalImage] as? UIImage {
            
            _imgVC3.image = image
            _imgVC3.layer.cornerRadius = _imgVC3.frame.height/2
            _imgVC3.clipsToBounds = true
            Singleton.shared[aux].Image = _imgVC3.image
        }
        self.dismiss(animated: false, completion: nil)
    }
    
}
