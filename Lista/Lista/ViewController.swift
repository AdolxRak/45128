//
//  ViewController.swift
//  singleton-lista-contactos
//
//  Created by Developer on 12/07/2018.
//  Copyright © 2018 adolx. All rights reserved.
//

import UIKit

class ViewController:UIViewController,UIImagePickerControllerDelegate,UINavigationControllerDelegate{
    
    
    var adr : UIImage!
    
    @IBOutlet weak var img: UIImageView!
    @IBOutlet weak var nome: UITextField!
    @IBOutlet weak var sobrenome: UITextField!
    @IBOutlet weak var tel: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
    }
    
    @IBAction func OpenCamera(_ sender: Any) {
        if UIImagePickerController.isSourceTypeAvailable(.camera){
            let imagePicker = UIImagePickerController()
            imagePicker.delegate = self
            imagePicker.allowsEditing = false
            imagePicker.sourceType = UIImagePickerControllerSourceType.camera
            self.present(imagePicker,animated: false,completion: nil)
            
        }
        
        
    }
    
    
    @IBAction func OpenImages(_ sender: Any) {
        let imagePicker = UIImagePickerController()
        imagePicker.delegate = self
        imagePicker.allowsEditing = false
        imagePicker.sourceType = UIImagePickerControllerSourceType.photoLibrary
        self.present(imagePicker,animated: false,completion: nil)
        
        
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        
        if let image = info[UIImagePickerControllerOriginalImage] as? UIImage {
            adr = image
            img.image = image
            img.layer.cornerRadius = img.frame.height/2
            img.clipsToBounds = true
        }
        self.dismiss(animated: false, completion: nil)
    }
    
    @IBAction func Btn(_ sender: Any) {
        var adx = 0
        
        Singleton.shared.append(Singleton(Nome : nome.text!  , Sobrenome : sobrenome.text! , Tel : tel.text! , Image : adr))
        
        Singleton.shared[adx].count += 1;
        
        adx += 1
        
        performSegue(withIdentifier: "t", sender: nil)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
}

