//
//  ViewController2.swift
//  singleton-lista-contactos
//
//  Created by Developer on 12/07/2018.
//  Copyright © 2018 a. All rights reserved.
//

import UIKit

class ViewController2: UIViewController , UITableViewDelegate , UITableViewDataSource {
        
    
    
   
    
    
    public func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        return(Singleton.shared.count)
    }
    public func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        let cell = UITableViewCell(style: UITableViewCellStyle.subtitle, reuseIdentifier: "Cell")
        if Singleton.shared[indexPath.row].Image != nil
        {
            cell.textLabel?.text = Singleton.shared[indexPath.row].Nome + " " + Singleton.shared[indexPath.row].Sobrenome
            cell.imageView?.image =  Singleton.shared[indexPath.row].Image
            cell.detailTextLabel?.text = "Tel: " + Singleton.shared[indexPath.row].Tel
        }
        else
        {
            cell.textLabel?.text = Singleton.shared[indexPath.row].Nome + " " + Singleton.shared[indexPath.row].Sobrenome
            cell.detailTextLabel?.text = "Tel: " + Singleton.shared[indexPath.row].Tel
        }
        return (cell)
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let storyBoard = UIStoryboard(name: "Main", bundle: nil)
        let dvc3 = storyBoard.instantiateViewController(withIdentifier: "ViewController3") as! ViewController3
        
        dvc3.nome3 = Singleton.shared[indexPath.row].Nome
        dvc3.sobrenome3 = Singleton.shared[indexPath.row].Sobrenome
        dvc3.tel3 = Singleton.shared[indexPath.row].Tel
        if (!(Singleton.shared[indexPath.row].Image == nil)){
            dvc3.img3 = Singleton.shared[indexPath.row].Image!
            dvc3.aux = indexPath.row
        }
        self.navigationController?.pushViewController(dvc3, animated: true)
    }
    @IBAction func GoGrid(_ sender: Any) {
        let storyBoard = UIStoryboard(name: "Main", bundle: nil)
        let go = storyBoard.instantiateViewController(withIdentifier: "Grid") as! Grid
        self.navigationController?.pushViewController(go, animated: true)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        
    }
    
    
}
